import { createApp } from 'vue'
import App from './App.vue'
import 'vant/lib/index.css'
import router from './router'
import Vant from 'vant';
import store from './store'
import './index.css'

const app = createApp(App)
app.use(router).use(Vant).use(store).mount('#app')
