import { createRouter, createWebHashHistory } from 'vue-router'

const HelloWorld = () => import('../components/HelloWorld.vue')
const colorwebsite = () => import('../view/colorwebsite.vue')
const filterweb = () => import('../view/filterweb.vue')

export default createRouter({
  // 指定路由模式
  history: createWebHashHistory(),
  // 路由地址
  routes: [
    {
      path: '/',
      component: HelloWorld
    },
    {
      path: '/colorwebsite',
      name: 'colorwebsite',
      component: colorwebsite
    },
    {
      path: '/fliterweb',
      name: 'fliterweb',
      component: filterweb
    }
  ]
})